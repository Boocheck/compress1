import numpy as np
import os
from scipy.ndimage import imread
from scipy.misc import imsave
import tensorflow as tf
import random
import itertools
import model


def probas_to_freq(array, decimals=2):
    ndarray = np.array(array)
    rounded = (ndarray * (10 ** decimals)).astype(int) + 1
    return rounded if isinstance(array, np.ndarray) else rounded.tolist()


def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def image_generator(path, mode="patch", size=(64,64,3), rand=True):

    filenames = os.listdir(path)
    i=0
    while True:
        filename = random.choice(filenames) if rand else filenames[i]
        i= (i+1)%len(filenames)
        filename = os.path.join(path, filename)
        if os.path.isfile(filename):
            array = imread(filename)
            if mode == "patch":
                h, w, c = array.shape
                y = random.randrange(0, h - size[0] + 1)
                x = random.randrange(0, w - size[1] + 1)
                z = random.randrange(0, c - size[2] + 1)
                yield array[y:y + size[0], x:x + size[1], z:z + size[2]]
            elif mode == "whole":
                yield array





def dir2ds(path, mode="patch", size=(64, 64, 3)):
    return tf.data.Dataset.from_generator(lambda : image_generator(path, mode, size), tf.float32, list(size))

def stop_condition(loss, patience, max_iter = None):
    if max_iter is not None and len(loss) >= max_iter:
        return True

    if len(loss) <= patience:
        return False



    min_hist = min(loss[:-patience])
    min_recent = min(loss[-patience:])
    return min_recent >= min_hist






if __name__ == "__main__":
    with tf.Session() as sess:
        ds = dir2ds("C:\\Users\\Przemas\\Downloads\\professional_train\\train")
        iter = ds.make_one_shot_iterator()
        next_element = iter.get_next()
        image = sess.run(next_element)
        x = 0


