import arithmeticcoding
import sys
import random
python3 = sys.version_info.major >= 3







def compress(freqs, inp, bitout):
    enc = arithmeticcoding.ArithmeticEncoder(bitout)
    while True:
        symbol = inp.read(1)
        if len(symbol) == 0:
            break
        symbol = symbol[0] if python3 else ord(symbol)
        enc.write(freqs, symbol)
    enc.write(freqs, 256)  # EOF
    enc.finish()  # Flush remaining code bits

def decompress(freqs, bitin, out):
    dec = arithmeticcoding.ArithmeticDecoder(bitin)
    while True:
        symbol = dec.read(freqs)
        if symbol == 256:  # EOF symbol
            break
        out.write(bytes((symbol,)) if python3 else chr(symbol))


flawless_symbol_nums = []
for i in range(1 , 260):
    is_ok=True

    for j in range(1,50,3):

        seq_len = j
        sym_num = i
        freqs = arithmeticcoding.SimpleFrequencyTable([1] * sym_num)

        # encode
        input_symbols = [random.randrange(sym_num) for __ in range(seq_len) ]
        f = open("lol.bin", "bw")
        output_bits = arithmeticcoding.BitOutputStreamInMemory()
        enc = arithmeticcoding.ArithmeticEncoder(output_bits)
        for s in input_symbols:
            enc.write(freqs, s)
        enc.finish()
        output_bits.close()
        f.close()

        # decode
        f = open("lol.bin", "br")
        input_bits = arithmeticcoding.BitInputStreamInMemory(output_bits.output)
        output_symbols = []
        dec = arithmeticcoding.ArithmeticDecoder(input_bits)
        for ___ in range(len(input_symbols)):
            s = dec.read(freqs)
            output_symbols.append(s)
        f.close()

        if input_symbols != output_symbols:
            is_ok = False
            break

    if is_ok:
        flawless_symbol_nums.append(i)
print(flawless_symbol_nums)
print(flawless_symbol_nums==list(range(1,260)))





#
#
#
# freqs = arithmeticcoding.SimpleFrequencyTable([1,1])
#
# input_symbols = [1,0,1,0,1]
# output_bits = arithmeticcoding.BitOutputStreamInMemory()
# enc = arithmeticcoding.ArithmeticEncoder(output_bits)
# for s in input_symbols:
#     enc.write(freqs, s)
# enc.finish()
# output_bits.close()
#
# input_bits = arithmeticcoding.BitInputStreamInMemory(output_bits.output)
# output_symbols = []
# dec = arithmeticcoding.ArithmeticDecoder(input_bits)
# for ___ in range(len(input_symbols)):
#     s = dec.read(freqs)
#     output_symbols.append(s)
#
# print(output_symbols == input_symbols)
#
