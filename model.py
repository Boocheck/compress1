import arithmeticcoding as ac
import tensorflow as tf
import numpy as np
import itertools
import utils
from immetrics import psnr, msssim
from datetime import datetime
import math



def weights(shape, name=None, initializer=tf.random_normal_initializer(0, 0.01)):
    return tf.get_variable(name, shape, tf.float32, initializer)

def batch_norm(tensor, name=None):
    means, vars = tf.nn.moments(tensor, [0], keep_dims=True)
    scale = tf.Variable(tf.ones(means.shape))
    shift = tf.Variable(tf.zeros(means.shape))
    return tf.nn.batch_normalization(tensor, means, vars, shift, scale, 1e-10)



def mask(shape, mask_type="A", space_dims=(0, 1, 2), name="mask"):
    result = np.zeros(shape)
    for tup in np.ndindex(*shape):
        value = 0.0
        for idx, dim in enumerate(space_dims):
            if idx != len(space_dims) - 1:
                if tup[dim] < result.shape[dim] // 2:
                    value = 1.0
                    break
                elif tup[dim] > result.shape[dim] // 2:
                    value = 0.0
                    break
            else:
                if (mask_type == "A" and tup[dim] < result.shape[dim] // 2) or (
                                mask_type == "B" and tup[dim] <= result.shape[dim] // 2):
                    value = 1.0
        result[tup] = value
    return tf.constant(result, dtype=tf.float32, name=name)


def conv2d(kernel, filters, stride, padding="SAME", name="conv2d"):
    def conv2d_result(input_tensor):
        with tf.name_scope(name):
            input_channels = input_tensor.shape[-1]
            w = weights(kernel + [input_channels, filters], name=name + "weights")
            b = weights([filters], name=name + "bias")
            conv_result = tf.nn.conv2d(input_tensor, w, [1, stride, stride, 1], padding, name=name) + b
        return conv_result

    return conv2d_result


def mconv3d(kernel, filters, stride, padding="SAME", mask_type="A", space_dims=(0, 1, 2), name="mconv3d"):
    def mconv3d_result(input_tensor):
        with tf.name_scope(name):
            input_channels = input_tensor.shape[-1]
            w_shape = kernel + [input_channels, filters]
            m = mask(w_shape, mask_type, space_dims, name + "mask")
            w = weights(w_shape, name=name + "weights")
            b = weights([filters], name=name + "bias")
            conv_Result = tf.nn.conv3d(input_tensor, w * m, [1, stride, stride, stride, 1], padding, name=name) + b
        return conv_Result, [w,b]

    return mconv3d_result


def conv3d(kernel, filters, stride, padding="SAME", name="conv3d"):
    def conv2d_result(input_tensor):
        with tf.name_scope(name):
            input_channels = input_tensor.shape[3]
            w = weights(kernel + [input_channels, filters], name=name + "weights")
            b = weights([filters], name=name + "bias")
            conv_result = tf.nn.conv2d(input_tensor, w, [1, stride, stride, 1], padding, name=name) + b
        return conv_result
    return conv2d_result


def deconv2d(kernel, filters, stride, output_shape, padding="SAME", name="deconv2d"):
    def deconv2d_result(input_tensor):
        with tf.name_scope(name):
            input_channels = input_tensor.shape[3]
            w = weights(kernel + [filters, input_channels], name=name + "weights")
            b = weights([filters], name=name + "bias")
            deconv_result = tf.nn.conv2d_transpose(input_tensor, w, output_shape, [1, stride, stride, 1], padding,
                                                   name=name) + b
        return deconv_result

    return deconv2d_result


def depth2space(kernel, filters, stride, blocksize, padding="SAME", name="depth_to_space"):
    def deconv2d_result(input_tensor):
        with tf.name_scope(name):
            input_channels = input_tensor.shape[3]
            effective_filters = filters*blocksize**2 if blocksize>1 else filters
            w = weights(kernel + [input_channels, effective_filters], name=name + "weights")
            b = weights([effective_filters], name=name + "bias")
            conv_result = tf.nn.conv2d(input_tensor, w, [1, stride, stride, 1], padding, name=name) + b
            d2s_result = tf.depth_to_space(conv_result, blocksize, name=name + "d2s") if blocksize > 1 else conv_result
        return d2s_result

    return deconv2d_result


def enc_dec(conf, name="enc_dec", dec="deconv"):

    conf = [list(x) for x in conf]

    bottleneck_poolings = 0
    for x in conf:
        bottleneck_poolings += 1 if x[3]==2 else 0
    bottleneck_channels = conf[-1][2]
    input_channels = 3


    def enc_dec_result(input_tensor):
        with tf.name_scope(name):

            tensors = [input_tensor]
            channels = [input_tensor.shape[-1]]
            current_pooling = 0
            middle_parts = []
            output_parts = []

            # shortcut from input to bottleneck
            adapter = tf.space_to_depth(input_tensor, 2**bottleneck_poolings)
            adapter = conv2d([1, 1], bottleneck_channels, 1, name="to_bottleneck_input")(adapter)
            middle_parts.append(adapter)

            # encoder
            for i,f in enumerate(conf):
                new_channels= f[2]
                if f[4] == "sigm":
                    activation = tf.sigmoid
                elif f[4] == "relu":
                    activation = tf.nn.relu
                elif f[4] == "leak":
                    activation = tf.nn.leaky_relu
                elif f[4] == "elu":
                    activation = tf.nn.elu
                elif f[4] == "tanh":
                    activation = tf.nn.tanh
                elif f[4] == "none":
                    activation = tf.identity
                else:
                    raise ValueError("zla funkcja aktywacji")

                new_layer = activation(conv2d(f[0:2], new_channels, f[3], name="conv2d_" + str(i))(tensors[-1]))

                # new_layer = batch_norm(new_layer)

                current_pooling += 1 if f[3] == 2 else 0
                block_size = 2**(bottleneck_poolings-current_pooling)
                adapter = tf.space_to_depth(new_layer, block_size) if block_size>1 else new_layer
                adapter = conv2d([1,1], bottleneck_channels, 1, name="to_bottleneck_" + str(i))(adapter)

                channels.append(new_channels)
                tensors.append(new_layer)
                middle_parts.append(adapter)

            middle = tensors[-1]

            # add shortcut connections to bottleneck
            for shortcut in middle_parts[:-1]:
                middle = middle + shortcut

            @tf.custom_gradient
            def round_gradient_passed(x):
                def grad(dy):
                    return dy  # return gradient unchanged

                return tf.round(x), grad

            # apply better rounding
            middle = tf.sigmoid(middle)
            middle = round_gradient_passed(middle)
            # tensors[-1] = tf.round(tensors[-1])
            tensors[-1] = middle

            # shortcut from bottleneck to output
            adapter = tf.depth_to_space(middle, 2 ** current_pooling)
            adapter = conv2d([1, 1], input_channels, 1, name="to_output_bottleneck")(adapter)
            output_parts.append(adapter)

            # decoder
            for i in list(reversed(range(len(tensors))))[1:]:
                t = tensors[i]
                ch = channels[i]
                f = conf[i]

                if dec == "deconv":
                    if f[4] == "sigm":
                        activation = tf.sigmoid
                    elif f[4] == "relu":
                        activation = tf.nn.relu
                    elif f[4] == "leak":
                        activation = tf.nn.leaky_relu
                    elif f[4] == "elu":
                        activation = tf.nn.elu
                    elif f[4] == "tanh":
                        activation = tf.nn.tanh
                    elif f[4] == "none":
                        activation = tf.identity
                    else:
                        raise ValueError("zla funkcja aktywacji")

                    last = activation(deconv2d(f[0:2], ch, f[3], tf.shape(t), name="deconv2d_" + str(i))(tensors[-1]))
                elif dec == "d2s":
                    last = tf.nn.leaky_relu(depth2space(f[0:2], ch, 1, f[3], name= "d2s_" + str(i))(tensors[-1]))
                else:
                    raise ValueError("wrong decoder type")

                current_pooling -= 1 if f[3] == 2 else 0
                block_size = 2 ** current_pooling
                adapter = tf.depth_to_space(last, block_size) if block_size>1 else last
                adapter = conv2d([1,1], input_channels, 1, name="to_output_" + str(i))(adapter)

                # last = batch_norm(last)
                tensors.append(last)
                output_parts.append(adapter)

            output = tensors[-1]
            for shortcut in output_parts[:-1]:
                output = output + shortcut
            output=tf.nn.relu(output)

        return middle, output

    return enc_dec_result


def image_modeling(name="image_modelling"):
    def image_modeling_result(input_4d):
        with tf.name_scope(name):
            # mconv1, v1 =  mconv3d([3, 3, 3], 16, 1, name="mconv1")(input_4d)
            # mconv1 = tf.nn.leaky_relu(mconv1)
            # mconv2, v2 = mconv3d([3, 3, 3], 16, 1, mask_type="B", name="mconv2")(mconv1)
            # mconv2 = tf.nn.leaky_relu(mconv2)
            # mconv3, v3 = mconv3d([3, 3, 3], 16, 1, mask_type="B", name="mconv3")(mconv2)
            # mconv3 = tf.nn.leaky_relu(mconv3)
            mconv4, v4 = mconv3d([3, 3, 3], 2, 1, mask_type="B", name="mconv4")(input_4d)

        return mconv4, v4

    return image_modeling_result


def entropy_encode(label_cube, probas_cube, bitout, dim_order=(0, 1, 2)):
    enc = ac.ArithmeticEncoder(bitout)

    for ndindex in itertools.product(*map(lambda x: list(range(label_cube.shape[x])), dim_order)):
        symbol = np.argmax(label_cube[ndindex])
        probas = probas_cube[ndindex].tolist()
        enc.write(ac.SimpleFrequencyTable(utils.probas_to_freq(probas)), symbol)

    # enc.write(freqs, 256)  # EOF
    enc.finish()  # Flush remaining code bits


def entropy_decode(out_label_cube, bitin, get_probas, dim_order=(0, 1, 2)):
    dec = ac.ArithmeticDecoder(bitin)

    for ndindex in itertools.product(*map(lambda x: list(range(out_label_cube.shape[x])), dim_order)):
        # Decode and write one byte
        probas = get_probas(out_label_cube, ndindex)
        freques = ac.SimpleFrequencyTable(utils.probas_to_freq(probas).tolist())
        symbol = dec.read(freques)
        out_label_cube[ndindex + (symbol,)] = 1.0

class Model:
    def __init__(self, ds_iterator, ds_handle, conf, decoder_type):

        self.ds_iterator = ds_iterator
        self.ds_handle = ds_handle
        # 4d batch of images [b, h, w, c]
        self.next_elem = ds_iterator.get_next()
        # next_elem = tf.cast(next_elem, tf.float64)

        # basic graph
        self.encoded, self.decoded = enc_dec(conf=conf, dec=decoder_type)(self.next_elem)
        self.encoded_transposed = tf.transpose(self.encoded, [0, 3, 1, 2])
        self.encoded_4d = tf.expand_dims(self.encoded_transposed, -1)

        self.labels_cm = tf.concat([self.encoded_4d, 1-self.encoded_4d], -1)
        self.codes_logits, self.codes_modeling_weights = image_modeling()(self.encoded_4d)
        self.codes_probas = tf.nn.softmax(self.codes_logits)

        # training graph
        lr = 1e-5
        self.global_step = tf.Variable(0, trainable=False)
        self.reset_global_step = tf.assign(self.global_step, 0)
        self.decayed_lr = tf.train.exponential_decay(lr, self.global_step, 5000, 0.9)
        self.loss = tf.losses.mean_squared_error(self.next_elem, self.decoded)
        self.minimize = tf.train.AdamOptimizer(self.decayed_lr).minimize(self.loss, global_step=self.global_step)
        self.loss_cm = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=self.codes_logits, labels=self.labels_cm))
        self.minimize_cm = tf.train.AdamOptimizer(self.decayed_lr).minimize(self.loss_cm, var_list=self.codes_modeling_weights, global_step=self.global_step)
        self.loss_both = self.loss + self.loss_cm
        self.minimize_both = tf.train.AdamOptimizer(self.decayed_lr).minimize(self.loss_both, global_step=self.global_step)

        # saving
        self.saver = tf.train.Saver()

        # initialization
        self.init = tf.global_variables_initializer()

        # summaries
        self.summary_loss = tf.summary.scalar('loss', self.loss)
        self.summary_loss_cm = tf.summary.scalar('loss_cm', self.loss_cm)
        self.summary_loss_both = tf.summary.scalar('loss_both', self.loss_both)
        self.summary_lr = tf.summary.scalar('lr', self.decayed_lr)
        self.summary_probas_hist = tf.summary.histogram('cm_probas_hist', self.codes_probas[:,:,:,:,0])
        self.summary_logits_hist = tf.summary.histogram('cm_logits_hist', self.codes_logits[:, :, :, :, 0])
        self.summary_labels_hist = tf.summary.histogram('cm_labels_hist', self.labels_cm[:, :, :, :, 0])
        self.summary_input_image = tf.summary.image("input", self.next_elem[0:1])
        self.summary_output_image = tf.summary.image("output", self.decoded[0:1])
        self.summary_codes_mean = tf.summary.scalar("codes_mean", tf.reduce_mean(self.encoded))

        self.summary_all = tf.summary.merge_all()





    def train(self, train_iter, test_iter, patience=50, sequential=True, checkpoint_path=None, event_path=None, time_prefix=None, verbose=True, max_iters=100, sess=None):


        loss=[]
        loss_cm=[]
        loss_both=[]
        summary_writer = tf.summary.FileWriter(event_path + "/{}train".format(time_prefix), tf.get_default_graph())

        loss_test = []
        loss_cm_test = []
        loss_both_test = []
        summary_writer_test = tf.summary.FileWriter(event_path + "/{}test".format(time_prefix), tf.get_default_graph())

        if sess is None:
            session = tf.Session()
        else:
            session = sess

        _, train_handle, test_handle = session.run([self.init, train_iter.string_handle(), test_iter.string_handle()])


        if sequential:
            i = 0
            while True:
                # train encoder-decoder for one epoch
                for _ in range(1000):
                    session.run(self.minimize, {self.ds_handle: train_handle})

                def evaluate(name, handle, loss_list, writer):
                    ev_loss, ev_image, ev_decoded, summary_buffer = session.run([self.loss, self.next_elem, self.decoded, self.summary_all], {self.ds_handle: handle})
                    loss_list.append(np.asscalar(ev_loss))
                    if verbose:
                        print("{:5} loss: {:.4}, PSNR: {:5.2f}[{:5.2f}], MS-SSIM: {:.2f}[{:.2f}]"
                              .format(name,
                                      loss_list[-1],
                                      psnr(ev_image*255, ev_decoded*255),
                                      psnr(np.round(ev_image * 255), np.round(ev_decoded * 255)),
                                      msssim(ev_image*255, ev_decoded*255 ),
                                      msssim(np.round(ev_image * 255), np.round(ev_decoded * 255)))
                              )

                    writer.add_summary(summary_buffer)

                print(i)
                evaluate("TRAIN", train_handle, loss, summary_writer)
                evaluate("TEST", test_handle, loss_test, summary_writer_test)
                print()


                if utils.stop_condition(loss,patience,max_iters):
                    break
                i+=1

            i=0
            session.run(self.reset_global_step)
            while True:

                # train code modeling for one epoch
                for _ in range(1000):
                    session.run(self.minimize_cm, {self.ds_handle: train_handle})

                def evaluate(name, handle, loss_list, writer):
                    ev_loss_cm, ev_codes_probas, ev_labels_cm, summary_buffer = session.run([self.loss_cm, self.codes_probas, self.labels_cm, self.summary_all], {self.ds_handle: handle})
                    loss_list.append(np.asscalar(ev_loss_cm))
                    acc = (np.argmax(ev_codes_probas, -1)==np.argmax(ev_labels_cm, -1)).mean()
                    if verbose:
                        print("{} loss cm: {}, acc: {}".format(name, loss_list[-1], acc))

                    writer.add_summary(summary_buffer)

                print(i)
                evaluate("TRAIN", train_handle, loss_cm, summary_writer)
                evaluate("TEST", test_handle, loss_cm_test, summary_writer_test)
                print()

                if utils.stop_condition(loss_cm, patience, max_iters):
                    break
                i+=1
        else:
            i=0
            while True:
                for _ in range(1000):
                    session.run(self.minimize_both, {self.ds_handle: train_handle})

                def evaluate(name, handle, loss_list, writer):
                    l, l_cm, l_both, summary_buffer = session.run([self.loss, self.loss_cm, self.loss_both, self.summary_all], {self.ds_handle:handle})
                    l, l_cm, l_both = np.asscalar(l), np.asscalar(l_cm), np.asscalar(l_both)
                    loss_list.append(l_both)
                    if verbose:
                        print("{} loss both: {} \n loss: {} ({}) \n loss_cm: {} ({})"
                              .format(name, l_both, l, l/l_both, l_cm, l_cm/l_both))
                    writer.add_summary(summary_buffer)

                print(i)
                evaluate("TRAIN", train_handle, loss_both, summary_writer)
                evaluate("TEST", test_handle, loss_both_test, summary_writer_test)
                print()

                if utils.stop_condition(loss_both, patience, max_iters):
                    break
                i+=1

        if checkpoint_path is not None:
            self.saver.save(session, checkpoint_path + "/model.ckpt")

        if sess is None:
            session.close()

        return loss, loss_cm, loss_both, loss_test, loss_cm_test, loss_both_test






#
# def encoder(name="conv_encoder"):
#     def enc_layer(input_tensor):
#         with tf.name_scope(name):
#             conv1 = tf.layers.conv2d(input_tensor, 1, [3, 3], 2, "valid", activation=tf.nn.relu, name="conv1")
#             conv2 = tf.layers.conv2d(conv1, 3, [3, 3], 2, "valid", activation=tf.nn.relu, name="conv2")
#             conv3 = tf.layers.conv2d(conv2, 3, [3, 3], 2, "valid", activation=tf.nn.relu, name="conv3")
#             print(conv1.shape, conv2.shape, conv3.shape)
#         return conv3
#
#     return enc_layer
#
#
# def decoder(name="conv_decoder"):
#     def dec_layer(input_tensor):
#         with tf.name_scope(name):
#             deconv1 = tf.layers.conv2d_transpose(input_tensor, 3, [3, 3], 2, "valid", activation=tf.nn.relu,
#                                                  name="deconv1")
#             deconv2 = tf.layers.conv2d_transpose(deconv1, 3, [3, 3], 2, "valid", activation=tf.nn.relu,
#                                                  name="deconv2")
#             deconv3 = tf.layers.conv2d_transpose(deconv2, 1, [3, 3], 2, "valid", activation=tf.nn.relu,
#                                                  name="deconv3")
#             print(deconv1.shape, deconv2.shape, deconv3.shape)
#         return deconv3
#
#     return dec_layer
