import os
import json
import datetime
import matplotlib.pyplot as plt
import csv


class Performance:
    def __init__(self, psnr, msssim, bpp, date, name):
        self.psnr = psnr
        self.msssim = msssim
        self.bpp = bpp
        self.date = date
        self.name = name


def scan_dir(path):
    result = []
    for root, dirs, files in os.walk(path):
        for file in files:
            if file == "performance.json":
                full = os.path.join(root, file)
                dct = json.load(open(full, "r", encoding="utf-8"))
                result.append(Performance(
                    dct["psnr"],
                    dct["msssim"],
                    dct["bpp"],
                    datetime.date.fromtimestamp(os.path.getmtime(full)).strftime('%Y-%m-%d'),
                    full.split(os.sep)[-2]
                ))
    return result


mine = scan_dir("C:\\Coding\\python\\compress1")
compression_cc = [
    Performance(27.67, 0.964, 0.15, "2018-04-31", "TucodecTNGcnn4p"),
    Performance(27.09, 0.954, 0.15, "2018-04-31", "xvc"),
    Performance(30.89, 0.955, 0.15, "2018-04-31", "iipTiramisu"),
    Performance(30.76, 0.955, 0.15, "2018-04-31", "TucodecTNG"),
    Performance(30.43, 0.951, 0.15, "2018-04-31", "yfan"),
    Performance(30.24, 0.951, 0.15, "2018-04-31", "tangzhimin"),
    Performance(30.30, 0.950, 0.15, "2018-04-31", "Amnesiack"),
    Performance(28.77, 0.956, 0.15, "2018-04-31", "AmnesiackLite"),
    Performance(30.14, 0.948, 0.15, "2018-04-31", "MVGL"),
    Performance(29.64, 0.943, 0.15, "2018-04-31", "BPG"),
    Performance(25.66, 0.863, 0.15, "2018-04-31", "JPEG")
]

combined = mine + compression_cc
combined.sort(key=lambda x: x.psnr, reverse=True)

########### save json
json.dump(
    [x.__dict__ for x in combined],
    open("C:\\Coding\\python\\compress1\\performance_dump.json", "w", encoding="utf-8"),
    indent=4
)

########## save csv
writer = csv.writer(
    open("C:\\Coding\\python\\compress1\\performance_dump.csv", "w", encoding="utf-8", newline="")
)
writer.writerow(["psnr", "ms-mssim", "bpp", "data", "nazwa"])
for elem in combined:
    writer.writerow([elem.psnr, elem.msssim, elem.bpp, elem.date, elem.name])

########## plot
fig, (ax_psnr, ax_msssim) = plt.subplots(2)
ax_psnr.plot(
    [x.bpp for x in mine], [x.psnr for x in mine], "r+",
    [x.bpp for x in compression_cc], [x.psnr for x in compression_cc], "b+",
)
ax_psnr.set(xlabel="bpp", ylabel="psnr")
ax_msssim.plot(
    [x.bpp for x in mine], [x.msssim for x in mine], "r+",
    [x.bpp for x in compression_cc], [x.msssim for x in compression_cc], "b+",
)
ax_msssim.set(xlabel="bpp", ylabel="msssim")

plt.show()
