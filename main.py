import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from envs.tfenv.Lib.datetime import datetime

import arithmeticcoding as ac
import itertools
import model
import utils
import json
import immetrics
import os
from scipy.misc import imread, imsave

PERFORM_TRAINING = True
DECONV_FROM_QUANTIZED = False


def create_dataset(directory, patch=(64, 64, 3), take=None):
    def parse_decode(filename):
        image_string = tf.read_file(filename)
        image_decoded = tf.image.decode_png(image_string)
        return image_decoded

    def parse_crop(image):
        image_cropped = tf.random_crop(image, patch)
        image_float = tf.cast(image_cropped, tf.float32) / 255.0
        return image_float

    # A vector of filenames.
    filenames = map(lambda x: os.path.join(directory, x), os.listdir(directory))
    filenames = tf.constant(list(filenames))

    dataset = tf.data.Dataset.from_tensor_slices(filenames)
    dataset = dataset.map(parse_decode, num_parallel_calls=8)
    if take:
        dataset=dataset.take(take)
    # dataset = dataset.apply(tf.contrib.data.map_and_batch(parse_fn, 16, 8))
    dataset = dataset.cache()
    dataset = dataset.map(parse_crop, num_parallel_calls=8)
    return dataset


def main2():
    # mnist = tf.contrib.learn.datasets.load_dataset("mnist")
    # train_data = mnist.train.images.reshape([-1, 28, 28, 1]).astype(np.float32)  # Returns np.array
    # ds_train = tf.data.Dataset.from_tensor_slices(train_data).repeat().shuffle(1000).batch(8)
    # iter = ds_train.make_one_shot_iterator()

    train_path = "C:\\Users\\Przemas\\Downloads\\professional_train\\train"
    test_path = "C:\\Users\\Przemas\\Downloads\\professional_test\\test"
    train_size = 595
    test_size = 118

    # if PERFORM_TRAINING:
    #     train_gen = utils.image_generator("C:\\Users\\Przemas\\Downloads\\professional_train\\train", size=(32, 32, 3), rand=False)
    #     train_list = [next(train_gen) for _ in range(20000)]
    #     train_np = (np.stack(train_list) / 255.0).astype(np.float32)

    dec_type = "deconv"

    def conf_to_string(conf):
        return "_".join(map(lambda x: "-".join(map(lambda y: str(y), x)), conf))

    confs = [
        # (
        #     (3, 3, 24*4, 1, "leak"),
        #     (3, 3, 24*4, 2, "leak"),
        #     (3, 3, 24*4*4, 1, "leak"),
        #     (3, 3, 24*4*4, 2, "leak"),
        #     (1,1, 256,1,"leak"),
        # ),
        (
            (3, 3, 64, 1, "leak"),
            (3, 3, 64, 2, "leak"),
            (3, 3, 128, 1, "leak"),
            (3, 3, 128, 2, "leak"),
            (3, 3, 128, 1, "leak"),
            (3, 3, 128, 2, "leak"),
            (1, 1, 64, 1, "leak"),
        ),
        # (
        #     (3, 3, 128, 1, "leak"),
        #     (3, 3, 128, 1, "leak"),
        #     (3, 3, 128, 2, "leak"),
        #     (3, 3, 256, 1, "leak"),
        #     (3, 3, 256, 1, "leak"),
        #     (3, 3, 256, 2, "leak"),
        #     (3, 3, 512, 1, "leak"),
        #     (3, 3, 512, 1, "leak"),
        #     (3, 3, 512, 2, "leak"),
        #     (1, 1, 64, 1, "sigm")
        # ),
        # (
        #     (3, 3, 64, 1, "leak"),
        #     (3, 3, 64, 1, "leak"),
        #     (3, 3, 64, 2, "leak"),
        #     (3, 3, 128, 1, "leak"),
        #     (3, 3, 128, 1, "leak"),
        #     (3, 3, 128, 2, "leak"),
        #     (3, 3, 128, 1, "leak"),
        #     (3, 3, 128, 1, "leak"),
        #     (3, 3, 128, 2, "leak"),
        #     (3, 3, 256, 1, "leak"),
        #     (3, 3, 256, 1, "leak"),
        #     (3, 3, 256, 2, "leak"),
        #     (1, 1, 64, 1, "sigm")
        # ),
        # (
        #     (3, 3, 128, 1, "leak"),
        #     (3, 3, 128, 1, "leak"),
        #     (3, 3, 128, 2, "leak"),
        #     (3, 3, 256, 1, "leak"),
        #     (3, 3, 256, 1, "leak"),
        #     (3, 3, 256, 2, "leak"),
        #     (3, 3, 256, 1, "leak"),
        #     (3, 3, 256, 1, "leak"),
        #     (3, 3, 256, 2, "leak"),
        #     (3, 3, 512, 1, "leak"),
        #     (3, 3, 512, 1, "leak"),
        #     (3, 3, 512, 2, "leak"),
        #     (1, 1, 64, 1, "sigm")
        # )
    ]

    sequence_modes = [True]
    confs = {conf_to_string(x): x for x in confs}

    for conf in confs:
        for sequence_mode in sequence_modes:
            arch_name = "{}_{}".format(conf, sequence_mode)
            checkpoint_root = "tmp/models/" + arch_name
            event_root = "tmp/events"

            time_prefix = datetime.utcnow().strftime("%Y%m%d%H%M%S")

            with open("tmp/architectures.txt", "a") as file:
                file.writelines([time_prefix + " " + arch_name + "\n"])

            if not os.path.exists(checkpoint_root):
                os.makedirs(checkpoint_root)

            with tf.Session() as sess:
                # datasets
                train_ds = create_dataset(train_path).repeat().batch(32).prefetch(buffer_size=8)
                test_ds = create_dataset(test_path).repeat().batch(32).prefetch(buffer_size=8)
                string_handle = tf.placeholder(tf.string, [])
                iterator = tf.data.Iterator.from_string_handle(string_handle, train_ds.output_types, train_ds.output_shapes)
                train_iter = train_ds.make_one_shot_iterator()
                test_iter = test_ds.make_one_shot_iterator()

                test_handle = sess.run(test_iter.string_handle())

                # train
                if PERFORM_TRAINING:
                    m = model.Model(iterator, string_handle, conf=confs[conf], decoder_type=dec_type)
                loss, loss_cm, loss_both, loss_test, loss_cm_test, loss_both_test = m.train(train_iter, test_iter, 50,
                                                                                            sequence_mode, checkpoint_root,
                                                                                            event_path=event_root,
                                                                                            time_prefix=time_prefix,
                                                                                            max_iters=256,
                                                                                            sess=sess)
                json.dump(
                    {"loss": loss, "loss_cm": loss_cm, "loss_both": loss_both, "loss_test": loss_test,
                     "loss_cm_test": loss_cm_test,
                     "loss_both_test": loss_both_test},
                    open(checkpoint_root + "/loss.json", "w"))

                bpp = []
                ssim = []
                psnr = []
                psnr255 = []
                im_num = 0

                # for image in itertools.islice(utils.image_generator("C:\\Users\\Przemas\\Downloads\\professional_test\\test", mode="whole", rand=False), 0, 10):

                batch = sess.run(m.next_elem, {m.ds_handle: test_handle})
                for i in range(batch.shape[0]):
                    image = batch[i:i + 1, :, :, :]

                # string_handle = tf.placeholder(tf.string, [])
                # batch = (np.expand_dims(image, 0) / 255.0).astype(np.float32)
                # ds_image = tf.data.Dataset.from_tensor_slices(batch).batch(1)
                # iter = tf.data.Iterator.from_string_handle(string_handle, ds_image.output_types, ds_image.output_shapes)
                # image_handle = sess.run(ds_image.make_one_shot_iterator().string_handle())
                # m = model.Model(iter, string_handle, conf=confs[conf], decoder_type=dec_type)
                # m.saver.restore(sess, root_path + "/model.ckpt")

                # input_image, output_decoded= sess.run([m.next_elem, m.decoded])

                output_decoded, output_labels_cm, output_codes_probas, output_encoded = sess.run(
                    [m.decoded, m.labels_cm, m.codes_probas, m.encoded], {m.next_elem: image})

                if not os.path.exists(checkpoint_root + "/samples"):
                    os.makedirs(checkpoint_root + "/samples")

                # if DECONV_FROM_QUANTIZED:
                #    output_decoded = sess.run(m.decoded, {m.encoded: np.round(output_encoded)})

                imsave(checkpoint_root + "/samples/" + str(im_num) + ".png", output_decoded[0])

                stream = ac.BitOutputStreamInMemory()
                model.entropy_encode(output_labels_cm[0], output_codes_probas[0], stream)

                h, w = image.shape[1:3]
                bpp.append(len(stream.output) * 8 / (w * h))
                ssim.append(immetrics.msssim(output_decoded * 255, image * 255))
                psnr.append(immetrics.psnr(image, output_decoded))
                psnr255.append(immetrics.psnr(image * 255, output_decoded * 255))
                im_num += 1

                json.dump({
                    "bpp": np.asscalar(np.mean(bpp)),
                    "msssim": np.asscalar(np.mean(ssim)),
                    "psnr": np.asscalar(np.mean(psnr)),
                    "psnr255": np.asscalar(np.mean(psnr255))
                },

                open(checkpoint_root + "/performance.json", "w"))

            tf.reset_default_graph()


def main():
    # config
    scale_ds = False
    save_enc = True
    load_enc = False
    train_enc = True
    save_cm = True
    load_cm = False
    train_cm = True
    plt.ion()
    model_path = "tmp/model.ckpt"

    mnist = tf.contrib.learn.datasets.load_dataset("mnist")
    train_data = mnist.train.images.reshape([-1, 28, 28, 1]).astype(np.float32)  # Returns np.array
    test_data = mnist.test.images.reshape([-1, 28, 28, 1]).astype(np.float32)  # Returns np.array

    if scale_ds:
        ds_train = tf.data.Dataset.from_tensor_slices(train_data).map(
            lambda x: tf.image.resize_images(x, (40, 40))).repeat().shuffle(1000).batch(8)
        ds_test = tf.data.Dataset.from_tensor_slices(test_data).map(
            lambda x: tf.image.resize_images(x, (40, 40))).repeat().shuffle(1000).batch(8)
    else:
        ds_train = tf.data.Dataset.from_tensor_slices(train_data).repeat().shuffle(1000).batch(8)
        ds_test = tf.data.Dataset.from_tensor_slices(test_data).repeat().shuffle(1000).batch(8)

    iter = tf.data.Iterator.from_structure(ds_train.output_types, ds_train.output_shapes)
    iter_train_op = iter.make_initializer(ds_train)
    iter_test_op = iter.make_initializer(ds_test)

    m = model.Model(iter)
    encoded, decoded, quantized, codes_probas, saver, minimize, minimize_cm, init, next_elem, labels_cm = m.encoded, m.decoded, m.encoded, m.codes_probas, m.saver, m.minimize, m.minimize_cm, m.init, m.next_elem, m.labels_cm

    # drawing init
    n_r = n_c = 16
    fig, axs = plt.subplots(n_r, n_c)
    images = []
    plt.show()

    with tf.Session() as session:

        session.run([init, iter_train_op])

        if load_enc:
            saver.restore(session, model_path)

        for i in range(100 if train_enc else 0):
            if train_enc:
                for _ in range(1000):
                    session.run(minimize)

            # eval enc
            input_im, output_im, encoded_im = session.run([next_elem, decoded, encoded])
            rounded_im = np.round(encoded_im)
            output_im_rounded = session.run(decoded, {encoded: rounded_im})

            images.extend([input_im[0, :, :, 0], output_im[0, :, :, 0], output_im_rounded[0, :, :, 0]])
            print("{} epoch finished".format(i))

            # draw
            for i in range(n_r):
                for j in range(n_c):
                    if i * n_c + j < len(images):
                        axs[i, j].imshow(images[i * n_c + j])
            plt.draw()
            plt.pause(0.01)

            if save_enc:
                saver.save(session, model_path)

        for i in range(1000):
            if train_cm:
                for _ in range(1000):
                    session.run(minimize_cm)

            # eval cm
            output_codes_probas, output_labels_cm = session.run([codes_probas, labels_cm])
            memory_bit_stream = ac.BitOutputStreamInMemory()
            model.entropy_encode(output_labels_cm[0], output_codes_probas[0], memory_bit_stream)
            memory_bit_stream.close()
            print("number of bytes: {}".format(len(memory_bit_stream.output)))

            def calc_probas(label_codes, ndindex):
                single_channeled = label_codes[:, :, :, 0:1]
                back_to_batched = single_channeled.reshape([1] + list(single_channeled.shape))
                # back_to_batched = np.expand_dims(label_codes, 0)
                result_probas = session.run(codes_probas, {quantized: back_to_batched})
                # return np.array([0.5, 0.5])
                # return result_probas
                return result_probas[0][ndindex]

            probas_with_calc_probas_on_full_codes = np.zeros(output_labels_cm[0].shape)
            for ndindex in np.ndindex(output_labels_cm.shape[1:4]):
                calculated_probas = calc_probas(output_labels_cm[0], ndindex)
                probas_with_calc_probas_on_full_codes[ndindex] = calculated_probas

            fraction_of_probas_full_codes = (output_codes_probas[0] == probas_with_calc_probas_on_full_codes).astype(
                int).mean()
            print("fraction of probas matching on full codes: {}".format(fraction_of_probas_full_codes))

            probas_with_calc_probas_on_sequential_codes = np.zeros(output_labels_cm[0].shape)
            sequential_codes = np.zeros(output_labels_cm[0].shape)
            for ndindex in np.ndindex(output_labels_cm.shape[1:4]):
                calculated_probas = calc_probas(sequential_codes, ndindex)
                probas_with_calc_probas_on_sequential_codes[ndindex] = calculated_probas
                sequential_codes[ndindex] = output_labels_cm[0][ndindex]

            fraction_of_probas_seq_codes = (
                    output_codes_probas[0] == probas_with_calc_probas_on_sequential_codes).astype(int).mean()
            print("fraction of probas matching on seq codes: {}".format(fraction_of_probas_seq_codes))

            input_memory_bit_stream = ac.BitInputStreamInMemory(memory_bit_stream.output)
            decoded_codes = np.zeros(output_labels_cm[0].shape)
            model.entropy_decode(decoded_codes, input_memory_bit_stream, calc_probas)

            print("fraction of bits matching: {}".format((output_labels_cm[0] == decoded_codes).astype(int).mean()))
            # print("diff: {}".format(output_labels_cm[0] - decoded_codes))
            # print(output_labels_cm[0] == decoded_codes)

            # x = np.array(memory_bit_stream.output) - np.array(input_memory_bit_stream.input)


if __name__ == "__main__":
    main2()
